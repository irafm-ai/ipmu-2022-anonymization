import cv2
import numpy as np
import click
import glob 
import os
from tqdm import tqdm

@click.command()
@click.argument('CITYSCAPES-ROOT')
def main(cityscapes_root):
    orig_prefix, blur_prefix, mask_prefix = 'leftImg8bit', 'leftImg8bit_blurred', 'anonym_masks'
    orig_ext, blur_ext = 'png', 'jpg'

    orig_image_paths = glob.glob(f'{cityscapes_root}/{orig_prefix}/**/*.{orig_ext}', recursive=True)
    blur_image_paths_constr = [x.replace(orig_prefix, blur_prefix).replace(orig_ext, blur_ext) for x in orig_image_paths]
    all_exists = [os.path.exists(x) for x in blur_image_paths_constr]
    assert all(all_exists)

    mask_paths = [x.replace(orig_prefix, mask_prefix) for x in orig_image_paths]
    for orig_p, blur_p, mask_p in tqdm(zip(orig_image_paths, blur_image_paths_constr, mask_paths), total=len(orig_image_paths)):
        orig = cv2.cvtColor(cv2.imread(orig_p), cv2.COLOR_BGR2HSV)[...,2].astype('float32')
        blur = cv2.cvtColor(cv2.imread(blur_p), cv2.COLOR_BGR2HSV)[...,2].astype('float32')

        # get raw mask (heuristic)
        mask  = np.abs(cv2.blur(orig,(5,5))-cv2.blur(blur, (5,5)))
        mask  = cv2.blur(mask,(7,7))
        mask  = mask/np.max(mask)*255
        
        # threshold
        mask[mask<30] = 0
        mask[mask>0]  = 255

        # clean
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)))
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN,  cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)))

        dirname = os.path.dirname(mask_p)

        if not os.path.exists(dirname):
            os.makedirs(dirname)
        cv2.imwrite(mask_p, mask)

if __name__ == '__main__':
    main()