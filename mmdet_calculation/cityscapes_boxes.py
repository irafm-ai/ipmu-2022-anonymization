dataset_type = "CocoDataset"
data_root = "/raid/data/cityscapes/"
CLASSES = ("person", "rider", "car", "truck", "bus", "train", "motorcycle", "bicycle")

img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True
)
albu_train_transforms = [
    dict(
        type="ShiftScaleRotate",
        shift_limit=0.0625,
        scale_limit=0.1,
        rotate_limit=20,
        interpolation=1,
        p=0.5,
    ),
    dict(
        type="RandomBrightnessContrast",
        brightness_limit=[0.1, 0.3],
        contrast_limit=[0.1, 0.3],
        p=0.2,
    ),
    dict(
        type="OneOf",
        transforms=[
            dict(
                type="RGBShift",
                r_shift_limit=10,
                g_shift_limit=10,
                b_shift_limit=10,
                p=1.0,
            ),
            dict(
                type="HueSaturationValue",
                hue_shift_limit=20,
                sat_shift_limit=30,
                val_shift_limit=20,
                p=1.0,
            ),
        ],
        p=0.1,
    ),
    dict(type="JpegCompression", quality_lower=85, quality_upper=95, p=0.2),
    dict(type="ChannelShuffle", p=0.1),
    dict(
        type="OneOf",
        transforms=[
            dict(type="Blur", blur_limit=3, p=1.0),
            dict(type="MedianBlur", blur_limit=3, p=1.0),
        ],
        p=0.1,
    ),
]

train_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(type="LoadAnnotations", with_bbox=True,),
    dict(
        type="Resize",
        img_scale=[(608, 608), (416, 416)],
        keep_ratio=True,
        multiscale_mode="range",
    ),
    # dict(type="CutOut", n_holes=16, cutout_shape=[(5, 10), (5*4, 10*4)]), # added. 5 and 10 selected because 1 dim of anonymization mask is 16 x n - divided by resize factor, you will get 5 and 10 for each shape
    dict(
        type="Albu",
        transforms=albu_train_transforms,
        bbox_params=dict(
            type="BboxParams",
            format="pascal_voc",
            label_fields=["gt_labels"],
            min_visibility=0.0,
            filter_lost_elements=True,
        ),
        keymap={"img": "image", "gt_masks": "masks", "gt_bboxes": "bboxes"},
        update_pad_shape=False,
        skip_img_without_anno=True,
    ),
    # dict(
    #     type="AutoAugment",
    #     policies=[
    #         [
    #             dict(type="BrightnessTransform", prob=0.3, level=7),
    #             dict(type="ContrastTransform", prob=0.3, level=7),
    #             dict(type="EqualizeTransform", prob=0.3),
    #             dict(type="ColorTransform", prob=0.2, level=5),
    #             dict(type="Shear", prob=0.4, level=5),
    #             dict(
    #                 type="Rotate",
    #                 level=3,
    #                 img_fill_val=(124, 116, 104),
    #                 prob=0.5,
    #                 scale=1,
    #                 max_rotate_angle=20,
    #             ),
    #         ],
    #         [dict(type="Translate", level=7, prob=0.5, img_fill_val=(124, 116, 104)),],
    #     ],
    # ),
    dict(type="RandomFlip", flip_ratio=0.5),
    dict(type="Normalize", **img_norm_cfg),
    dict(type="Pad", size_divisor=32),
    dict(type="DefaultFormatBundle"),
    dict(type="Collect", keys=["img", "gt_bboxes", "gt_labels"]),
]
test_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(
        type="MultiScaleFlipAug",
        img_scale=(608, 608),
        flip=False,
        transforms=[
            dict(type="Resize", keep_ratio=True),
            dict(type="RandomFlip"),
            dict(type="Normalize", **img_norm_cfg),
            dict(type="Pad", size_divisor=32),
            dict(type="DefaultFormatBundle"),
            dict(type="Collect", keys=["img"]),
        ],
    ),
]
data = dict(
    samples_per_gpu=1,
    workers_per_gpu=2,
    train=dict(
        type="RepeatDataset",
        times=1,
        dataset=dict(
            type=dataset_type,
            ann_file=data_root + "annotations/instancesonly_filtered_gtFine_train.json",
            img_prefix=data_root + "leftImg8bit/train/",
            pipeline=train_pipeline,
            classes=CLASSES,
        ),
    ),
    val=dict(
        type=dataset_type,
        ann_file=data_root + "annotations/instancesonly_filtered_gtFine_val.json",
        img_prefix=data_root + "leftImg8bit/val/",
        pipeline=test_pipeline,
        classes=CLASSES,
    ),
    test=dict(
        type=dataset_type,
        ann_file=data_root + "annotations/instancesonly_filtered_gtFine_val.json",
        img_prefix=data_root + "leftImg8bit/val/",
        pipeline=test_pipeline,
        classes=CLASSES,
    ),
)
