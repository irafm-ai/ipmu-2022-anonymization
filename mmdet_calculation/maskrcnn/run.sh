data_prefix='/path/to/cityscapes/root' # TODO add path to the root of downloaded and anonymized dataset

datasets="leftImg8bit anonymized_with_black anonymized_with_permute anonymized_with_blur" # TODO generate anonymized dataset to this folders
# datasets="leftImg8bit" # test with original dataset

export CUDA_VISIBLE_DEVICES=0 # select GPU when n of gpus > 1
export MMDET_TRAIN_SCRIPT='/home/pedro/mmdetection/tools/' # TODO add path to mmdetection/tools/train.py script


for d in ${datasets}; do
        python ${MMDET_DIR}/train.py  mask_rcnn_r50.py \
                --cfg-options \
                    log_config.hooks.1.init_kwargs.name=1_maskrcnn_${d} \
                    data.train.dataset.img_prefix=${data_prefix}${d}/train \
                    train_pipeline.2.img_scale='[(1600, 800), (2048, 1024)]' \
                    test_pipeline.1.img_scale='(2048, 1024)' \
                    data.train.dataset.pipeline.2.img_scale='(2048, 1024)' \
                    data.val.pipeline.1.img_scale='(2048, 1024)' \
                    data.test.pipeline.1.img_scale='(2048, 1024)' \
                    data.samples_per_gpu=8 \
                    data.workers_per_gpu=8 \
                    evaluation.save_best='bbox_mAP' \
                    evaluation.metric='bbox' \
                --work-dir work_dir/1_maskrcnn_${d}
done
