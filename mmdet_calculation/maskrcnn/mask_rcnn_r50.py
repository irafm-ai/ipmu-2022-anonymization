_base_ = ["../../configs/_base_/models/mask_rcnn_r50_fpn.py", '../cityscapes_segmentation.py']
checkpoint_config = dict(interval=-1, save_last=True)

# TODO setup wandb account https://wandb.ai/home
log_config = dict(
    interval=1,
    hooks=[
        dict(type="TextLoggerHook"),
        dict(
            type="WandbLoggerHook",
            init_kwargs=dict(
                project="mmdet_anonymization",
                entity="your_wandb_username", # TODO
                name="mask_rcnn_r50",
            ),
        ),
    ],
)

# optimizer
optimizer = dict(type='SGD', lr=0.02, momentum=0.9, weight_decay=0.0001)
optimizer_config = dict(grad_clip=None)
# learning policy
lr_config = dict(
    policy='step',
    warmup='linear',
    warmup_iters=500,
    warmup_ratio=0.001,
    step=[8, 11])
runner = dict(type='EpochBasedRunner', max_epochs=12)


custom_hooks = [dict(type='NumClassCheckHook')]

dist_params = dict(backend='nccl')
log_level = 'INFO'
load_from = None
resume_from = None
workflow = [('train', 1)]
