data_prefix='/path/to/cityscapes/root' # TODO add path to the root of downloaded and anonymized dataset

datasets="leftImg8bit anonymized_with_black anonymized_with_permute anonymized_with_blur" # TODO generate anonymized dataset to this folders
# datasets="leftImg8bit" # test with original dataset

export CUDA_VISIBLE_DEVICES=0 # select GPU when n of gpus > 1
export MMDET_TRAIN_SCRIPT='/path/to/mmdetection/tools/train.py' # TODO add path to mmdetection/tools/train.py script

for d in ${datasets}; do
        python ${MMDET_DIR}/train.py  swin.py \
                --cfg-options \
                    log_config.hooks.1.init_kwargs.name=1_swin_${d} \
                    data.train.dataset.img_prefix=${data_prefix}${d}/train \
                    train_pipeline.2.img_scale='[(1333, 800),(1333,480)]' \
                    test_pipeline.1.img_scale='(1333, 800)' \
                    data.train.dataset.pipeline.2.img_scale='(1333,800)' \
                    data.val.pipeline.1.img_scale='(1333,800)' \
                    data.test.pipeline.1.img_scale='(1333,800)' \
                    data.samples_per_gpu=8 \
                    data.workers_per_gpu=8 \
                    evaluation.save_best='bbox_mAP' \
                    evaluation.metric='bbox' \
                --work-dir work_dir/1_swin_${d}
done
