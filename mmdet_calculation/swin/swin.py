_base_ = [
    "../cityscapes_segmentation.py",
    "../../configs/_base_/models/mask_rcnn_r50_fpn.py",
]

# TODO setup wandb account https://wandb.ai/home
checkpoint_config = dict(interval=-1, save_last=True)
log_config = dict(
    interval=1,
    hooks=[
        dict(type="TextLoggerHook"),
        dict(
            type="WandbLoggerHook",
            init_kwargs=dict(
                project="mmdet_anonymization",
                entity="your_wandb_username", # TODO
                name="yolov3_d53",
            ),
        ),
    ],
)
custom_hooks = [dict(type="NumClassCheckHook")]
log_level = "INFO"
load_from = None
resume_from = None
workflow = [("train", 1)]


pretrained = "https://github.com/SwinTransformer/storage/releases/download/v1.0.0/swin_tiny_patch4_window7_224.pth"  # noqa

model = dict(
    type="MaskRCNN",
    backbone=dict(
        _delete_=True,
        type="SwinTransformer",
        embed_dims=96,
        depths=[2, 2, 6, 2],
        num_heads=[3, 6, 12, 24],
        window_size=7,
        mlp_ratio=4,
        qkv_bias=True,
        qk_scale=None,
        drop_rate=0.0,
        attn_drop_rate=0.0,
        drop_path_rate=0.2,
        patch_norm=True,
        out_indices=(0, 1, 2, 3),
        with_cp=False,
        convert_weights=True,
        init_cfg=dict(type="Pretrained", checkpoint=pretrained),
    ),
    neck=dict(in_channels=[96, 192, 384, 768]),
)

img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True
)

# augmentation strategy originates from DETR / Sparse RCNN
optimizer = dict(
    type="AdamW",
    lr=0.0001,
    betas=(0.9, 0.999),
    weight_decay=0.05,
    paramwise_cfg=dict(
        custom_keys={
            "absolute_pos_embed": dict(decay_mult=0.0),
            "relative_position_bias_table": dict(decay_mult=0.0),
            "norm": dict(decay_mult=0.0),
        }
    ),
)
optimizer_config = dict(grad_clip=dict(max_norm=35, norm_type=2))
optimizer_config = dict(grad_clip=None)
# lr_config = dict(
#    policy="step", warmup="linear", warmup_iters=500, warmup_ratio=0.001, step=[8, 11]
#)
lr_config = dict(policy="step", gamma=0.1, step=[30, 33])

runner = dict(type="EpochBasedRunner", max_epochs=36)
