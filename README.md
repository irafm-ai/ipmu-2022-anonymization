1. Install mmdetection according to the [official instructions](https://github.com/open-mmlab/mmdetection/blob/master/docs/en/get_started.md).
2. Clone the mmdetection repository on local machine to get the `tools/train.py` script. Add it to the variable in the `run.sh` script.
3. Download the [Cityscapes dataset](https://www.cityscapes-dataset.com/)
4. Use the masks in the `anonym_masks` directory, the Cityscapes dataset and the `masks.py` script to create anonymized versions of the dataset. Let `--help` flag to guide you.
5. Alternatively you can use script called `initial_mask_creation_from_jpg.py` for the initial `anonym_masks` creation - you are free to experiment with it.
6. Create [wandb account](https://wandb.ai/) to be able to log the metrics.
7. Change lines marked with `TODO` in the `mmdet_calculation/{maskrcnn,yolov3,swin}/{run.sh,-model-name-.py}` files according to your setup.
8. Run the calculations with `run.sh` script in `mmdet_calculation/{maskrcnn,yolov3,swin}` directories.
