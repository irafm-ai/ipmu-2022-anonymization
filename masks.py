import cv2
import click
import glob
import os
from tqdm import tqdm
import numpy as np
from icecream import ic

import itertools
import more_itertools
import multiprocessing

class Anonymize:
    anonym_square_size = 16
    @staticmethod
    def forward(self, image, mask):
        raise NotImplementedError
    
    @staticmethod
    def id():
        raise NotImplementedError
        
    @staticmethod
    def _get_raw_boxes(mask):
        mask = np.uint8(mask[...,0])
        connectivity = 4  # 4 or 8 for conectivity type
        output = cv2.connectedComponentsWithStats(mask, connectivity, cv2.CV_32S)
        boxes = output[2][...,:-1] # x, y, w, h. Drop the area size
        return boxes[1:] # first item is background (whole image)

    @staticmethod    
    def get_boxes_divisible_by(divider, mask):
        max_h, max_w, _ = mask.shape
        boxes = Anonymize._get_raw_boxes(mask)
        boxes_divisible = []
        for x, y, w, h in boxes:
            w_residue = w % divider
            h_residue = h % divider
            if (x + w + w_residue) >= max_w:
                x = x - w_residue
            if (y + h + h_residue) >= max_h:
                y = y - h_residue
            w = w + divider - w_residue
            h = h + divider - h_residue
            for x_repeat in range(0, int(w / divider)):
                for y_repeat in range(0, int(h / divider)):
                    x_offset, y_offset = divider * x_repeat, divider * y_repeat
                    boxes_divisible.append((x + x_offset, y + y_offset, divider, divider))
        return boxes_divisible

class Blur(Anonymize):
    @staticmethod
    def forward(image, mask):
        boxes2anonym = Anonymize.get_boxes_divisible_by(Anonymize.anonym_square_size, mask)
        mask_boxes = np.zeros(mask.shape, dtype='bool')
        for x, y, w, h in boxes2anonym:
            mask_boxes[y:y+h, x:x+w] = True
        blur = cv2.blur(image, ksize=(Anonymize.anonym_square_size, )*2)
        result = np.multiply(blur, mask_boxes) + np.multiply(image,~mask_boxes)
        return result
    
    @staticmethod
    def id():
        return 'blur'

class Permute(Anonymize):
    seed = 42
    @staticmethod
    def forward(image, mask):
        mask = np.int8(mask)
        boxes2anonym = Anonymize.get_boxes_divisible_by(Anonymize.anonym_square_size, mask)
        for x, y, w, h in boxes2anonym:
            roi = image[y:y+h, x:x+w]
            roi_shape = roi.shape
            roi = np.reshape(roi, (roi_shape[0] * roi_shape[1] * roi_shape[2], ))
            np.random.seed(Permute.seed)
            roi = np.random.permutation(roi)
            roi = np.reshape(roi, roi_shape)
            image[y:y+h, x:x+w] = roi
        return image
            
    @staticmethod
    def id():
        return 'permute'

class Black(Anonymize):
    @staticmethod
    def forward(image, mask):
        mask = np.int8(mask)
        boxes2anonym = Anonymize.get_boxes_divisible_by(Anonymize.anonym_square_size, mask)
        for x, y, w, h in boxes2anonym:
            image[y:y+h, x:x+w] = 0
        return image

    @staticmethod
    def id():
        return 'black'

class Gray(Anonymize):
    @staticmethod
    def forward(image, mask):
        mask = np.int8(mask)
        boxes2anonym = Anonymize.get_boxes_divisible_by(Anonymize.anonym_square_size, mask)
        for x, y, w, h in boxes2anonym:
            image[y:y+h, x:x+w] = 127
        return image

    @staticmethod
    def id():
        return 'gray'

def load_transform_and_save(args):
    image_p, mask_p, out_p, anonym_method = args[0], args[1], args[2], args[3]
    image = cv2.imread(image_p).astype('float32')
    mask = cv2.imread(mask_p).astype('float32')

    anonym_f = more_itertools.one([x for x in Anonymize.__subclasses__() if x.id() == anonym_method])
    result = anonym_f.forward(image, mask)

    directory = os.path.dirname(out_p)
    try:
        os.makedirs(directory)
    except FileExistsError:
        pass
    cv2.imwrite(out_p, result)

    return out_p

@click.command()
@click.argument('CITYSCAPES-ROOT')
@click.argument('OUTPUT-PREFIX')
@click.option('--n-proc', default=multiprocessing.cpu_count() - 2, type=int, help='Number of CPU cores to use.')
@click.option('--anonym-method', default='blur', type=click.Choice([x.id() for x in Anonymize.__subclasses__()]))
def main(cityscapes_root, output_prefix, n_proc, anonym_method):
    ext = 'png'
    images_prefix, masks_prefix = 'leftImg8bit', 'anonym_masks'

    image_paths = glob.glob(f'{cityscapes_root}/{images_prefix}/**/*.{ext}', recursive=True)
    mask_paths = [x.replace(images_prefix, masks_prefix) for x in image_paths]
    masks_exist = [os.path.exists(x) for x in mask_paths]
    assert all(masks_exist)

    output_paths = [x.replace(images_prefix, output_prefix, 1) for x in image_paths]

    with multiprocessing.Pool(n_proc) as p:
        r = list(
            tqdm(
                p.imap(
                    load_transform_and_save,
                    zip(image_paths, mask_paths, output_paths, itertools.cycle((anonym_method,)))
                    ),
                total=len(image_paths)
                )
            )
        assert len(r) == len(image_paths)

if __name__ == '__main__':
    main()